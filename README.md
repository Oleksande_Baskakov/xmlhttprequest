# XMLHttpRequest
 >Hello everybody!!!

Here is my own Fetch API! It works as usual fetch but with implimentation of XMLHttpRequest
  
***
Example of use:
```
  const http = new HttpRequest({
    baseUrl: 'http://localhost:8000',
    headers: {contentType: undefined}
  });
```
to create an instance of HttpRequest you should call it as constructor and give it two arguments: baseUrl(necessary!!!) and headers(optional).
```
  http.get(`/files/picture.jpeg`, {
    responseType: 'arraybuffer',
    onDownloadProgress
  }).then(console.log)


  //output of console.log: data: ArrayBuffer(6687) {}
  //                       responseType: "arraybuffer"
  //                       status: 200
  //                       statusText: "OK"
  //                       url: "http://localhost:8000/files/picture.jpeg" 
```
post method can recieve 2 arguments: url, config.
instance can use such methods as "get", "post", "delete". Imitation of Responce will look like output in console.log in upper example.
```
  const form = new FormData();
  const http = new HttpRequest({
    baseUrl: 'http://localhost:8000'
  });
  form.append('myData', {name: 'Alex'});
  http.post('/upload', form, {
    onUploadProgress
  });
```
post method can recieve 3 arguments: url, data, config. Also, I tried and added the implementation of functions "onUploadProgress" and "onDownloadProgress". You can throw them to congif and they will called on uploading and downloading respectively.
***
Good luck and have a nice day!

